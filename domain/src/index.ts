import * as _utils from "./utils";

export const utils = _utils;

export function domainFn(): string {
  return "Called from domain";
}
