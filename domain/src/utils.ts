import * as winston from "winston";
import { z } from "zod";

type Parseable = string | boolean | number;
const _ = typeof 0;
type TypeOf = typeof _;

function parse(key: string, typeArg: TypeOf): Parseable {
  if (typeArg === "number") {
    return JSON.parse(key);
  }
  if (typeArg === "boolean") {
    return JSON.parse(key.toLowerCase());
  }
  if (typeArg === "string") {
    return key;
  }
  throw new Error("Can only parse boolean, number and strings");
}

interface GetVarOpts {
  key: string;
  _default?: Parseable;
  typeArg?: TypeOf;
}

export function getVar({ key, _default, typeArg }: GetVarOpts): Parseable {
  const res = process.env[key];
  if (res && _default) {
    return parse(res, typeof _default);
  }
  if (res && typeArg) {
    return parse(res, typeArg);
  }
  if (!res && _default) {
    return _default;
  }
  if (!_default && !typeArg) {
    throw new Error("Either typeArg or _default required");
  }
  throw new Error(`Environment Variable ${key} required`);
}

const Config = z.object({
  logLevel: z.union([
    z.literal("ERROR"),
    z.literal("WARN"),
    z.literal("INFO"),
    z.literal("DEBUG"),
  ]),
  sentryUrl: z.string().url(),
});

export const config = Config.parse({
  logLevel: getVar({ key: "LOG_LEVEL", _default: "DEBUG" }),
  sentryUrl: getVar({ key: "SENTRY_URL", typeArg: "string" }),
});

export function setupLogger(cfg: z.infer<typeof Config>): winston.Logger {
  const level = cfg.logLevel.toLowerCase();
  switch (level) {
    case "error":
    case "warn":
    case "info":
    case "debug":
      break;
    default:
      throw new Error(
        "Logger only accepts ERROR, WARN, INFO, and DEBUG levels"
      );
  }
  return winston.createLogger({
    transports: [
      new winston.transports.Console({
        level,
        format: winston.format.printf(
          ({ level: lvl, message }): string =>
            `${lvl.toUpperCase()}: ${message}`
        ),
      }),
    ],
  });
}
