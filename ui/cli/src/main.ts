import * as Sentry from "@sentry/node";
import { clientsFn } from "@ts-starter/clients";
import { domainFn, utils } from "@ts-starter/domain";

function main(): void {
  Sentry.init({
    dsn: utils.config.sentryUrl,
  });
  const logger = utils.setupLogger(utils.config);
  logger.info("CLI is starting...");
  logger.info(domainFn());
  logger.info(clientsFn());
}

main();
