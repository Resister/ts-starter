## TS-Node starter &nbsp; <img height="50px" style="margin-bottom: -12px" src=https://raw.githubusercontent.com/TypeStrong/ts-node/main/logo-icon.svg>

## Tooling  
### Base
- Package Manager: **yarn**
- Type Checking: **tsc**
- Formatting: **prettier**
- Linting: **eslint**
- Package Security: **yarn audit**
- Commit Formatter: **git-cz**
- Logging: **winston** 
- Error Monitoring: **sentry** 
- Data Validation: **zod**

## Monorepo Layout
- **clients/**  
  - Data Sources: sql, s3, graphql, rest api/json schema
- **domain/** 
  - Biz logic
- **ui/** 
  - User Interfaces (in the purest sense of the term):  Can be graphql, rest api, or cli

```
├── clients
│   ├── package.json
│   └── src
│       └── index.ts
├── domain
│   ├── package.json
│   └── src
│       ├── index.ts
│       └── utils.ts
├── package.json
├── readme.md
├── tsconfig.json
├── .eslintrc.yml
├── ui
│   └── cli
│       ├── package.json
│       ├── src
│       │   └── main.ts
│       └── yarn.lock
└── yarn.lock
```